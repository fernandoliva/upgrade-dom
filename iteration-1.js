//1.1
const $$boton = document.querySelector('.showme');
console.log($$boton);

//1.2
const $$titulo = document.querySelector('#pillado');
console.log($$titulo);

//1.3
const $$parrafos = document.querySelectorAll('p');
console.log($$parrafos);

//1.4
const $$pokemon = document.querySelectorAll('.pokemon');
console.log($$pokemon);

//1.5
const $$test = document.querySelectorAll('span[data-function="testMe"]');
console.log($$test);

//1.6
console.log($$test[2]);