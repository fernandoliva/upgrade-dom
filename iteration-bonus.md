##### Bucles
1. Tenemos dos alumnos que han obtenido las calificaciones siguientes:
Alumno1 -> 8;
Alumno2 -> 6;
Nos piden hacer una comparación entre las notas de los dos alumnos y
quedarnos con la nota mayor(imprimir por consola ese alumno con console.log()).
P.D.: Necesitaras un if.

2. Tenemos una relacion de alumnos de una clase que han sacado las notas siguientes.
const clase = [5, 7, 8, 6, 4, 5, 2, 1, 7, 9, 10]
Ahora nos piden imprimir por consola la nota mayor de la clase y la nota menor.
P.D.: Necesitaras un for y un if.

3. Con la misma clase del ejercicio anterior se solicita que conteis la cantidad de alumnos
que ha aprobado(han sacado 5 o mas).
P.D.: Necesitaras una variable contador, un bucle for y un if.

4. Nos proporcionan los datos de una clase de la siguiente manera:
const clase = [
    {
        nombre: "Juan",
        nota: 8
    },
    {
        nombre: "Miguel",
        nota: 8     
    },
    {
        nombre: "David",
        nota: 5     
    },
    {
        nombre: "Carlos",
        nota: 9     
    },
    {
        nombre: "Alvaro",
        nota: 9  
    }
];
4.1. Se nos solicita imprimir por pantalla (console.log) los nombres de todos los alumnos (Utilizar un bucle for).
4.2. Se nos solicita imprimir por consola (console.log) todas la informacion de la case obteniendo un resultado parecido a este:
alumno X tiene ha obtenido la nota Y. siendo X el nombre del alumno e Y la nota del alumno.
4.3. Se nos solicita imprimir por consola el nombre del alumno con la mayor nota
P.D.: Necesitas un for i un if. Tambien puedes guardar al alumno con mas nota en una variable.
4.4. Se nos solicita amplicar la ficha de cada alumno e introducirle un id, el primer alumno tendría el id:1, el segundo id:2, etc, ejemplo de como quedaria:
{
    nombre: "Juan",
    nota: 8,
    id:1
}
P.D.: Necesitas un for.
4.5. Se nos solicita crear una variable (array) que contenga solo los nombres de todos los alumnos. A esta variable la vamos a llamar alumnos.
P.D.: Necesitaremos un for.
4.6. Se nos socita añadir a la clase otros tres alumnos (a vuestro gusto) con la estructura:
{
    nombre: string,
    nota: numero,
    id: numero correlativo
}
###Funciones