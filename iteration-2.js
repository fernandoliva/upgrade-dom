//2.1
// const $$divVacio = document.createElement('div');
// $$divVacio.textContent = 'Prueba';
// document.body.appendChild($$divVacio);

//2.2
// const $$div2 = document.createElement('div');
// const $$p = document.createElement('p');
// $$div2.appendChild($$p);
// document.body.appendChild($$div2);

//2.3
// const $$div3 = document.createElement('div');
// for (let i = 0; i < 6; i++){
//     const $$p = document.createElement('p');
//     $$div3.appendChild($$p);
// }
// document.body.appendChild($$div3);

//2.4
// const $$dinamic = document.createElement('p');
// $$dinamic.innerText = 'Soy dinámico!';
// document.body.appendChild($$dinamic);

//2.5
//Inserta en el h2 con la clase .fn-insert-here el texto 'Wubba Lubba dub dub'.

// const $$wubba = document.querySelector('h2.fn-insert-here');
// $$wubba.innerText = 'Wubba Lubba dub dub';

//2.6
// const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];
// const $$ul = document.createElement('ul');

// for (let i = 0; i < apps.length; i++) {
//     const element = apps[i];
//     const $$li = document.createElement('li');
//     $$li.innerText = element;
//     $$ul.appendChild($$li);
// }

// document.body.appendChild($$ul);

//2.7
// const $$elements = document.querySelectorAll('.fn-remove-me');

// for (element of $$elements){
//     element.remove();
// }

//2.8
// let $$paragraph = document.createElement('p');
// $$paragraph.innerText = 'Voy en medio!';

// const $$div = document.querySelectorAll('div');
// document.body.insertBefore($$paragraph, $$div[1]);

//2.9
//Inserta p con el texto 'Voy dentro!', dentro de todos los div con la clase .fn-insert-here



const $$divs = document.querySelectorAll('div.fn-insert-here');
// console.log($$divs);

for (let div of $$divs){
    let $$p = document.createElement('p');
    $$p.innerText = 'Voy dentro!';
    div.appendChild($$p);
}
